<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mydatabase' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'sI1hz5B8e6DNwCnh' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{ajhagZv>%`^yW1vGg[Ts 7ZsP>W4.7e7fFuy}<h+`c)@5?M=L^ZQ-_NF&LjM0x2' );
define( 'SECURE_AUTH_KEY',  '+v9Nh%vVIbHqZp5azZFvwHd`4H}+2kUU^*A!p#0`BDb0dqlujAsYB*_V^)?B+NpX' );
define( 'LOGGED_IN_KEY',    'Qm`FPqAq5Z|Mt?FsLy3@4{h8dhE&->tHl&>#}W54cc^[!tcV&SffXvlR|Ig02^5_' );
define( 'NONCE_KEY',        'k]m6B%kcV<I_0kJm=;8c! E~8{=,[Uzq)[E$5_,KuiEm7u]~qKTz0j@CkuvY+):u' );
define( 'AUTH_SALT',        'Yw:4?#x* hg.%4A-ZNpR&|1h0aQ&@alh2;,-M Ovi3+fTxi*XgfwS/,4gH@FjKw/' );
define( 'SECURE_AUTH_SALT', '8{v%a`cGnFnO+}q!fbC/48}a.47l35sYKZr<n}vN 5>>cLvUx}3:[lw3:Lp!ar^c' );
define( 'LOGGED_IN_SALT',   'VQO^Sn:F_&;dC4P+F6aH3G`=]IBDp0Lf/6b8x=FFohr>.dXo7M/0S]RBLTE>z``D' );
define( 'NONCE_SALT',       '|$dX)U!XWXZBqrK)z:2]DB/,>|W-?Kk,w_B6h1c29x[LWyBmh~|3w+a*eudWfm9U' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
