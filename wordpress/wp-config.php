<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'k8s_db' );

/** MySQL database username */
define( 'DB_USER', 'admin33' );

/** MySQL database password */
define( 'DB_PASSWORD', 'sI1hz5B8e6DNwCnh' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b}/G>|2{yeUpQL(MHTX8Jjc)3=m]GIIGfHl>0(q&ZLinO{s)E_L ?HP4=;kR|;}0' );
define( 'SECURE_AUTH_KEY',  '_( h]UHr~)|[[4eW(+>#}?H10,.}O2b~<jMmLS7*f_Yg|z782L>w>/`,.},!k=y~' );
define( 'LOGGED_IN_KEY',    'FW}e:**4(LLfh00wM{Gu:(h{jN<^^Q{Cepa-):)Q]:SWLXKRk]i( c9emCw,-D6w' );
define( 'NONCE_KEY',        'y/TP*HRW/=wM#) r9rKp8Kyx~~?[LSdHDJ_Dn+dvmJ<<~G3%X>KL,)}K))M((]&h' );
define( 'AUTH_SALT',        '%EMw4O}VSmlXD<Q_in.r]HU<!Di9Ag+;#Kz>b%A(Z^qgLey}r kK,bw7/{L#-y;h' );
define( 'SECURE_AUTH_SALT', '_c2Myf9)r:r?8(-R?%:2:pSM?+_7C$>ZaE!&rAlL+zf;lU|2}?h X1qqew_I?YAg' );
define( 'LOGGED_IN_SALT',   '_eW8i u1TV8m`3-D9.]p=Zjgr*y#22d.n<qRT-wo*R((V=X(7Wsd9(*xuO{Lfb*U' );
define( 'NONCE_SALT',       'N4nV6Xs;+]$lC>SD{1l4zh!BwJ!5vvfiseSku6S5H_65{Up>Q<O@*gv^Xv,8 K*)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
